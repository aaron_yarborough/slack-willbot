﻿using System.Collections.Generic;
using System.Configuration;

namespace OkCoolBot
{
    public static class AppSettings
    {
        public static string Target => ConfigurationManager.AppSettings["target"];
        public static string BotUser => ConfigurationManager.AppSettings["botUser"];
        public static double ReactionChance => double.Parse(ConfigurationManager.AppSettings["reactionChance"]) / 100;
        public static double MessageChance => double.Parse(ConfigurationManager.AppSettings["messageChance"]) / 100;
        public static string OkCoolReaction => ConfigurationManager.AppSettings["okCoolReaction"];
        public static string AuthToken => ConfigurationManager.AppSettings["authToken"];
        public static double WittyMessageChance => double.Parse(ConfigurationManager.AppSettings["wittyMessageChance"]) / 100;

        public static Dictionary<string, string> Willisms = new Dictionary<string, string>
        {
            ["dude"] = "Hippy words such as 'dude' are not cceptable in the workplace. Please use alternatives, such as 'comrade', 'peer', 'pal' or 'chap'",
            ["yada"] = $"@{Target} I didn't know you could yodel!",
            ["yadda"] = $"@{Target} I didn't know you could yodel!",
            ["woo"] = "CELLLLLEBRATE GOOD TIMES, CMON!",
            ["yo"] = "Yo dawg :cool:",
            ["fuck off"] = "That's no way to speak to a colleague."
        };

        public static string[] WittyMessages = new[]
        {
            "Nice socks today @will",
            "Yadda yadda yadda",
            "Time for standup?",
            "You'd look good with a monocle"
        };
    }
}
