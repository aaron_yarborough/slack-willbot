﻿using System;

namespace OkCoolBot
{
    class Program
    {
        static void Main(string[] args)
        {
            var bot = new OkCoolBot(AppSettings.AuthToken);
            Console.ReadLine();
        }
    }
}
