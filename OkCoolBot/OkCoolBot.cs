﻿using SlackAPI;
using System;
using Flurl.Http;

namespace OkCoolBot
{
    public class OkCoolBot
    {
        readonly SlackSocketClient client;
        readonly Random random;
        readonly string apiToken;

        User _botUser;
        User _targetUser;
        Channel _directChannel;

        #region Constructors
        public OkCoolBot()
        {
            random = new Random();
        }

        public OkCoolBot(string apiToken) 
            : this()
        {
            client = new SlackSocketClient(apiToken);
            this.apiToken = apiToken;

            Init(apiToken);
        }
        #endregion

        void Init(string apiToken)
        {
            client.TestAuth((callback) =>
            {
                if (!callback.ok)
                    throw new Exception($"Authentication failed: {callback.error}");

                Console.WriteLine("Successfully authenticated with token");
            });

            client.Connect((connected) =>
            {
                _botUser = client.Users.Find(u => u.name == AppSettings.BotUser);
                _targetUser = client.Users.Find(u => u.name == AppSettings.Target);

                client.JoinDirectMessageChannel((callback) =>
                {
                    _directChannel = callback.channel;

                    Console.WriteLine("Joined direct message channel with target");
                }, _targetUser.id);

                Console.WriteLine("Succesfully connected to Slack");
                Console.WriteLine($"Bot User ID: {_botUser.id}");
                Console.WriteLine($"Target User ID: {_targetUser.id}");
            });

            client.OnMessageReceived += (message) =>
            {
                var sender = client.Users.Find(u => u.id == message.user);
                var channel = client.Channels.Find(c => c.id == message.channel);
                var isDirectMessage = _directChannel != null && message.channel == _directChannel.id;

                if (isDirectMessage)
                {
                    if (message.text.StartsWith("\"") && message.text.EndsWith("\""))
                    {
                        var messageTextStripped = message.text.Replace("\"", "");
                        client.SendMessage((messageRec) => { }, _directChannel.id, messageTextStripped);
                    }
                }

                if (sender.name == AppSettings.Target)
                {
                    if (random.NextDouble() < AppSettings.MessageChance)
                        client.SendMessage((messageReceived) => { }, message.channel, "ok cool");

                    if (random.NextDouble() < AppSettings.ReactionChance)
                        AddReactionToMessage(message.channel, message.ts, AppSettings.OkCoolReaction);

                    if (random.NextDouble() < AppSettings.WittyMessageChance)
                    {
                        var wittyMessage = AppSettings.WittyMessages[random.Next(0, AppSettings.WittyMessages.Length)];
                        client.SendMessage((messageRec) => { }, message.channel, wittyMessage);
                    }

                    foreach (var willism in AppSettings.Willisms)
                    {
                        if (message.text.Contains(willism.Key))
                        {
                            client.SendMessage((messageRec) => { }, message.channel, willism.Value);
                        }
                    }
                }
            };
        }

        async void AddReactionToMessage(string channelId, DateTime timeStamp, string reactionName)
        {
            var resp = await "https://slack.com/api/reactions.add"
                .PostUrlEncodedAsync(new { token = this.apiToken, channel = channelId, timestamp = timeStamp.ToProperTimeStamp(true), name = reactionName })
                .ReceiveJson();
        }
    }
}
